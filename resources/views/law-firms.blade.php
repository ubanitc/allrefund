@extends('layouts.home')
@section('content')
<section class="section" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s" data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                <img src="{{ asset('assets/images/left-image.png') }}" class="rounded img-fluid d-block mx-auto" alt="App">
            </div>
            <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                <div class="left-heading">
                    <h5>Investigative Services for Law Firms</h5>
                </div>
                <div class="left-text">
                    <p>Allrefund is a trusted expert to attorneys and law firms, providing sound advice to help you find creative and legal ways to come out ahead of your adversary – not off-the-shelf ideas in the legal gray area that could threaten your lawsuit.<br><br></p>
                    <p></p><h5>We Find Creative Ways to Come Out Ahead of Your Adversary</h5><p></p>
                    <p>
                        Whether you are looking to determine if a lawsuit is worth filings, need to gain leverage during the course of negotiations, find some “dirt” on an expert witness or you need to gather facts and information relating to your lawsuit, we are here to help.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="hr"></div>
            </div>
        </div>
    </div>
</section>
@endsection
