<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Template Mo">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <title>Home - Allrefund</title>
    <link href="{{ asset('img/logo.png') }}" rel="icon">

    <!--

ART FACTORY

https://templatemo.com/tm-537-art-factory

-->
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/templatemo-art-factory.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl-carousel.css') }}">

</head>

<body>

<!-- ***** Preloader Start ***** -->
<div id="preloader">
    <div class="jumper">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!-- ***** Preloader End ***** -->


<!-- ***** Header Area Start ***** -->
<header class="header-area header-sticky background-header whiteheader">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <div class="col-md-3">
                        <a href="{{ route('home') }}" class="logo">
                            <img src="{{ asset('img/logo.png') }}" style="width:84%;" alt="Allrefund"/>
                        </a>
                    </div>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav" style="color: black !important;">

                        <li class="submenu" style="color: black !important;">
                            <a href="#about" style="color: black !important;">About Us</a>
                            <ul>
                                <li><a href="#about" style="color: black !important;">Why we are different</a></li>
                                <li><a href="#frequently-question" style="color: black !important;">How we are different</a></li>
                            </ul>
                        </li>
                        <li class="submenu" style="color: black !important;">
                            <a href="#services" style="color: black !important;">Who we help</a>
                            <ul style="color: black !important;">
                                <li><a style="color: black !important;" href="{{ route('individuals') }}">Individuals</a></li>
                                <li><a style="color: black !important;" href="{{ route('investors') }}">Investors</a></li>
                                <li><a style="color: black !important;" href="{{ route('law-firms') }}">Law firms</a></li>
                                <li><a style="color: black !important;" href="{{ route('businesses') }}">Businesses</a></li>
                            </ul>
                        </li>
                        <li style="color: black !important;" class="scroll-to-section"><a style="color: black !important;" href="{{ route('blacklist') }}">Blacklists</a></li>

                        <li style="color: black !important;" class="scroll-to-section"><a style="color: black !important;" href="{{ route('login') }}">Log In</a></li>
                        <li  class="scroll-to-section">
                            <div id="google_translate_element" class=""></div>
                        </li>
                    </ul>
                    <a class='menu-trigger'>
                        <span>Menu</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->

@yield('content')

<!-- ***** Footer Start ***** -->
<script src="{{ asset('assets/js/jquery-2.1.0.min.js') }}"></script>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12">
                <p class="copyright">Copyright &copy; 2024 Allrefund

            </div>
            <div class="col-lg-5 col-md-12 col-sm-12">
                <p class="copyright">
                <div id="google_translate_element" class=""></div>
                <p>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery -->

<!-- Bootstrap -->
<script src="{{ asset('assets/js/popper.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

<!-- Plugins -->
<script src="{{ asset('assets/js/owl-carousel.js') }}"></script>
<script src="{{ asset('assets/js/scrollreveal.min.js') }}"></script>
<script src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/js/imgfix.min.js') }}"></script>

<!-- Global Init -->
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/65f70a879317c5437128c76c/1hp6g291o';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'en'
        }, 'google_translate_element');
    }
</script>

<script>
    function calcExbtc(obj) {
        fixPeriod(obj);
        var amt = $('#btcamount').val();
        var period = $('#period').val();

        var examt = obj * 0.01 * amt * period;

        $('#btcexpectedamount').val(examt);

    }

    function calcExbtc2(obj) {
        fixPeriod2(obj);
        var amt = $('#btcamount').val();
        var period = $('#period').val();

        var examt = obj * 0.01 * amt * period;

        $('#btcexpectedamount').val(examt);

    }

    function fixPeriod(obj) {


        var period = $('#period').val();

        if (period < 90) {
            $('#period').val('90');
        }




    }

    function fixPeriod2(obj) {


        var period = $('#period').val();

        if (period < 90 && period.length !== 2) {
            $('#period').val('90');
        }




    }

    function calcExeth() {

        var amt = $('#ethamount').val();

        var examt = amt * 2;

        $('#ethexpectedamount').val(examt);

    }

    function calcExltc() {

        var amt = $('#ltcamount').val();

        var examt = amt * 2;

        $('#ltcexpectedamount').val(examt);

    }

    function calcExbch() {

        var amt = $('#bchamount').val();

        var examt = amt * 2;

        $('#bchexpectedamount').val(examt);

    }

    function calcExxlm() {

        var amt = $('#xlmamount').val();

        var examt = amt * 2;

        $('#xlmexpectedamount').val(examt);

    }

    function toggleWallet() {

        var type = $('#cointype').val();

        if (type.toLowerCase() === "btc") {

            $('#btcholder').show();

            $('#ethholder').hide();

            $('#btccashholder').hide();

            $('#ltcholder').hide();
            $('#stlholder').hide();

        }

        if (type.toLowerCase() === "eth") {

            $('#ethholder').show();

            $('#btcholder').hide();

            $('#btccashholder').hide();

            $('#ltcholder').hide();
            $('#stlholder').hide();

        }

        if (type.toLowerCase() === "bch") {

            $('#btccashholder').show();

            $('#ethholder').hide();

            $('#btcholder').hide();

            $('#ltcholder').hide();
            $('#stlholder').hide();

        }

        if (type.toLowerCase() === "ltc") {

            $('#ltcholder').show();

            $('#btccashholder').hide();

            $('#ethholder').hide();

            $('#btcholder').hide();
            $('#stlholder').hide();

        }
        if (type.toLowerCase() === "stl") {

            $('#ltcholder').hide();

            $('#btccashholder').hide();

            $('#ethholder').hide();

            $('#btcholder').hide();
            $('#stlholder').show();

        }

    }
</script>



<script>
    function getText() {

        var text = "{no} people just deposited,{nobtc}.{dec} BTC deposited,{nobtc}.{dec} BTC processed for withdrawal,{bigno} visitors are active on this website,{nobtc}.{dec} BTC withdrawal complete,{noeth}.{dec} BTC processed for withdrawal,{noeth}.{dec} BTC received,{noeth}.{dec} BTC deposited,{nobtc}.{dec} BTC deposited,{no} people just deposited,{noeth}.{dec} BTC deposited,1.{dec} BTC deposited,{bigno} currently active visitors";

        vartextarr = text.split(",");

        var rnd = getRand(1, vartextarr.length);

        rnd = rnd - 1;

        var text = vartextarr[rnd].replace("{no}", getRand(2, 10));

        text = text.replace("{noa}", getRand(6, 20));

        text = text.replace("{nobtc}", getRand(1, 3));

        text = text.replace("{noltc}", getRand(30, 200));

        text = text.replace("{nobch}", getRand(4, 40));

        text = text.replace("{noeth}", getRand(8, 80));

        text = text.replace("{dec}", getRand(040, 989));

        text = text.replace("{bigno}", getRand(60, 90));

        return text;

    }

    function showFlash(txt) {

        $.notify(txt, "info");

    }

    function getRand(min, max) {

        return Math.floor(Math.random() * max) + min

    }

    setTimeout(

        function() {

            //showFlash(getText())



        }, 5000);

    setInterval(

        function() {

            //showFlash(getText())



        }, 20000);
</script>
<script>
    function submitrev() {
        var fname = $('#ffname').val();
        var femail = $('#femail').val();
        var fmsg = $('#fmsg').val();
        if (fname != "" && femail != "" && fmsg != "") {
            swal("Thanks for submitting a comment. We are currently reviewing. Your comment should be up any moment from now");
            $('#ffname').val('');
            $('#femail').val('');
            $('#fmsg').val('');
        } else {
            swal("Please fill all details");
        }
    }
</script>




</body>

</html>
