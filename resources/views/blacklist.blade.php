@extends('layouts.home')
@section('content')
    <section class="section" id="about">
        <div class="container">
            <div class="row">

                <div class="right-text col-lg-12 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="" style="text-align:center;">
                        <h5>Black Listed Companies &amp; Websites</h5>
                    </div>
                    <div class="" style="text-align:center;">
                        <p>In front of you is a collection of scam companies and various websites that we have compiled and organized.<br>Search this database and check if you were exposed to one of these scammers.<br>You might think that you have lost but what if you have been scammed?</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
        <div class="container" style="background-color:#34abf2;">
            <div class="row" style="padding:6%;">

                <div class="owl-carousel owl-theme owl-loaded owl-drag">

                    <div class="owl-stage-outer"><div class="owl-stage" style="width: 4683px; transform: translate3d(-1170px, 0px, 0px); transition: all 0s ease 0s;"><div class="owl-item cloned" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist5.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">10Brokers</h5>

                                    <p>10Brokers is an online Forex and CFD brokerage firm that commenced its business in the year 2018.</p>
                                    <a href="blacklistitem.php?id=5" class="main-button">Read More</a>
                                </div></div><div class="owl-item cloned" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist6.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">10CFDs</h5>

                                    <p>10CFDS is a Forex brokerage located in Belize. According to the brokerage, clients are extended extremely generous leverage of up to 1:200.</p>
                                    <a href="blacklistitem.php?id=6" class="main-button">Read More</a>
                                </div></div><div class="owl-item cloned" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist7.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">Forise-invest.com (10 Markets)</h5>

                                    <p>Forise-invest is an online trading service provider that offers trading services in various assets like Forex, Stocks, Commodities, and others.</p>
                                    <a href="blacklistitem.php?id=7" class="main-button">Read More</a>
                                </div></div><div class="owl-item cloned" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist9.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">Jetcapitals.com (12Trader)</h5>

                                    <p>Jetcapitals.com is a brokerage that claims to be formed by the best global financial market experts.</p>
                                    <a href="blacklistitem.php?id=9" class="main-button">Read More</a>
                                </div></div><div class="owl-item active" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist1.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">GoldsteinInvest</h5>

                                    <p>GoldsteinInvest is a Forex / Crypto CFD broker that was opened in February 2018.</p>
                                    <a href="blacklistitem.php?id=1" class="main-button">Read More</a>
                                </div></div><div class="owl-item active" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist2.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">Skycapital (Mcoin)</h5>

                                    <p></p>
                                    <a href="blacklistitem.php?id=2" class="main-button">Read More</a>
                                </div></div><div class="owl-item active" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist3.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">Bitll.com (1000Extra)</h5>

                                    <p>Bitll.com is an offshore Forex broker offering multiple financial assets for trading.</p>
                                    <a href="blacklistitem.php?id=3" class="main-button">Read More</a>
                                </div></div><div class="owl-item" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist4.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">100XFX</h5>

                                    <p>100XFX is an offshore Forex and CFD broker that offers to trade with multiple financial assets.</p>
                                    <a href="blacklistitem.php?id=4" class="main-button">Read More</a>
                                </div></div><div class="owl-item" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist5.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">10Brokers</h5>

                                    <p>10Brokers is an online Forex and CFD brokerage firm that commenced its business in the year 2018.</p>
                                    <a href="blacklistitem.php?id=5" class="main-button">Read More</a>
                                </div></div><div class="owl-item" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist6.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">10CFDs</h5>

                                    <p>10CFDS is a Forex brokerage located in Belize. According to the brokerage, clients are extended extremely generous leverage of up to 1:200.</p>
                                    <a href="blacklistitem.php?id=6" class="main-button">Read More</a>
                                </div></div><div class="owl-item" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist7.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">Forise-invest.com (10 Markets)</h5>

                                    <p>Forise-invest is an online trading service provider that offers trading services in various assets like Forex, Stocks, Commodities, and others.</p>
                                    <a href="blacklistitem.php?id=7" class="main-button">Read More</a>
                                </div></div><div class="owl-item" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist9.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">Jetcapitals.com (12Trader)</h5>

                                    <p>Jetcapitals.com is a brokerage that claims to be formed by the best global financial market experts.</p>
                                    <a href="blacklistitem.php?id=9" class="main-button">Read More</a>
                                </div></div><div class="owl-item cloned" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist1.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">GoldsteinInvest</h5>

                                    <p>GoldsteinInvest is a Forex / Crypto CFD broker that was opened in February 2018.</p>
                                    <a href="blacklistitem.php?id=1" class="main-button">Read More</a>
                                </div></div><div class="owl-item cloned" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist2.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">Skycapital (Mcoin)</h5>

                                    <p></p>
                                    <a href="blacklistitem.php?id=2" class="main-button">Read More</a>
                                </div></div><div class="owl-item cloned" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist3.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">Bitll.com (1000Extra)</h5>

                                    <p>Bitll.com is an offshore Forex broker offering multiple financial assets for trading.</p>
                                    <a href="blacklistitem.php?id=3" class="main-button">Read More</a>
                                </div></div><div class="owl-item cloned" style="width: 262.667px; margin-right: 30px;"><div class="item service-item" style="min-height: 528px;">
                                    <div class="icon">
                                        <i><img src="{{ asset('img/blacklist4.png') }}" alt=""></i>
                                    </div>
                                    <h5 class="service-title">100XFX</h5>

                                    <p>100XFX is an offshore Forex and CFD broker that offers to trade with multiple financial assets.</p>
                                    <a href="blacklistitem.php?id=4" class="main-button">Read More</a>
                                </div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>
            </div>
        </div>
    </section>
@endsection
