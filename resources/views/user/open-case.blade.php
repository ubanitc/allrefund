@extends('layouts.userdash')
@section('content')

    <div class="welcome-area" style="min-height: 100vh !important;" id="welcome">

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container" style="background-color:white; border:solid 1px white;text-align:center;">

                <div class="row">
                    <span style="width: 100%;text-align: center;color: #dc3545;border-bottom: solid 1px #b0b0de;margin-bottom: 4px;"><strong>Open case</strong></span>

                </div>

                <div class="contact-form" style="background-color:white;text-align: center !important;padding-left: 4px;padding-right: 2px;">
                    <form id="contact" action="{{ route('user.open-case-post') }}" method="post" style="text-align: center !important;" data-dashlane-rid="7b7d9941705c2e4a" data-form-type="contact">
                        @csrf

                        <div class="" style="text-align: center !important;">
                            <div class="row" style="margin:0px;">
                                <div class="col-lg-6 col-md-6 col-sm-6" style="text-align: center !important;">
                                    <fieldset style="text-align: center !important;">
                                        <input name="firstname" style="text-align: center !important;" type="text" class="contact-field" id="firstname" placeholder="First Name" required="" value="{{ Auth::user()->first_name }}" disabled="" data-dashlane-rid="a6de427ceb0ef5c6" data-kwcachedvalue="Carter Holden" data-kwimpalastatus="asleep" data-kwimpalaid="1710616310432-0" data-form-type="name,first">
                                    </fieldset>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6" style="text-align: center !important;">
                                    <fieldset style="text-align: center !important;">
                                        <input name="lastname" style="text-align: center !important;" type="text" class="contact-field" id="lastname" placeholder="Last Name" required="" value="{{ Auth::user()->first_name }}" disabled="" data-dashlane-rid="ecdbcd514a8cb32f" data-kwcachedvalue="Kaye Sampson" data-kwimpalastatus="asleep" data-kwimpalaid="1710616310432-1" data-form-type="name,last">
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row" style="margin:0px;">
                                <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center !important;">
                                    <fieldset style="text-align: center !important;">
                                        <input name="title" style="text-align: center !important;" type="text" class="contact-field" id="subject" placeholder="Subject" required data-dashlane-rid="079cd15360186ca5" data-form-type="other">
                                    </fieldset>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center !important;">
                                    <fieldset style="text-align: center !important;">

                                        <textarea name="message" placeholder="Full description of case..." style="text-align: center !important;border-radius: 25px;" type="text" class="contact-field" id="message" required data-dashlane-rid="70833eb9e8c26b15" data-form-type="other"></textarea>
                                    </fieldset>
                                </div>
                            </div>



                            <div class="col-lg-12">
                                <fieldset>
                                    <input type="submit" id="form-submit" name="form-submit" class="main-button" value="Submit" style="background-color: #ca95fb;color: #011c23;" data-dashlane-rid="b4ce97b67e429f96" data-form-type="action">
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>

@endsection
