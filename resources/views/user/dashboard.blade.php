@extends('layouts.userdash')

@section('content')

    <div class="welcome-area" id="welcome">

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container" style="background-color:white; border:solid 1px white;text-align:center;">

                <div class="row">
                    <span style="width: 100%;text-align: center;color: #dc3545;border-bottom: solid 1px #b0b0de;margin-bottom: 20px;"><strong>Dashboard</strong></span>

                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-3 col-sm-6 col-6">
                        <a href="{{ route('user.all-cases') }}" class="mini-box">
                            <i><img src="{{ asset('assets/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>{{ $open->count() }}</strong>
                            <span>Open Cases.</span>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-3 col-sm-6 col-6">
                        <a href="{{ route('user.all-cases') }}" class="mini-box">
                            <i><img src="{{ asset('assets/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>{{ $closed->count() }}</strong>
                            <span>Closed Cases.</span>
                        </a>
                    </div>
                </div>
                <div class="icon">
                    <i><img src="{{ asset('assets/images/featured-item-01.png') }}" alt=""></i>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-3 col-sm-6 col-6">
                        <a href="{{ route('user.all-cases') }}" class="mini-box">
                            <i><img src="{{ asset('assets/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>{{ $case->count() }}</strong>
                            <span>Total Cases.</span>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-3 col-sm-6 col-6">
                        <a href="{{{ route('user.open-case') }}}" class="mini-box">
                            <i><img src="{{ asset('assets/images/work-process-item-01.png') }}" alt=""></i>
                            <strong>Open</strong>
                            <span>New Case.</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>

@endsection
