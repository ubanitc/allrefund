@extends('layouts.userdash')
@section('content')
    <div class="welcome-area" style="min-height: 100vh !important;" id="welcome">

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container" style="background-color:white; border:solid 1px white;text-align:center;">

                <div class="row">
                    <span style="width: 100%;text-align: center;color: #dc3545;border-bottom: solid 1px #b0b0de;margin-bottom: 4px;"><strong>All cases</strong></span>


                </div>
                @if (session('success-case'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success-case') }}
                    </div>
                @endif
                <div class="contact-form" style="text-align: center !important;padding-left: 4px;padding-right: 4px; padding-top:2px;">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Subject</th>
                                <th scope="col">Status</th>
                                <th scope="col">Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            @forelse( $cases as $index => $case)
                                <tr>
                                    <th scope="row"><a href="{{ route('user.all-cases-page', $case->id) }}" style="color:white;"></a>{{ $index + 1 }}</th>
                                    <td><a href="{{ route('user.all-cases-page', $case->id) }}" style="color:white;">{{ $case->title }}</a> </td>
                                    <td><a href="{{ route('user.all-cases-page', $case->id) }}" style="color:white;">{{ $case->status }}</a> </td>
                                    <td><a href="{{ route('user.all-cases-page', $case->id) }}" style="color:white;">{{ $case->created_at }}</a></td>
                                </tr>
                            @empty
                                <td colspan="4"><a style="color:white;">No Cases on Recorded Yet</a> </td>

                            @endforelse


                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
@endsection
