@extends('layouts.userdash')
@section('content')
    <div class="welcome-area" style="min-height: 100vh !important;" id="welcome">

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container" style="background-color:white; border:solid 1px white;text-align:center;">

                <div class="row">
                    <span style="width: 100%;text-align: center;color: #dc3545;border-bottom: solid 1px #b0b0de;margin-bottom: 4px;"><strong>Case {{ $case->id }}</strong></span>


                </div>
                @if (session('success-case'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success-case') }}
                    </div>
                @endif
                <div class="contact-form" style="text-align: left !important;padding-left: 4px;padding-right: 4px; padding-top:2px;">
                    <h3><strong>Title:</strong> {{ $case->title }}</h3>
                    <br>
                    <p>Status: {{ $case->status }}</p>
                    <p><strong>Description:</strong>{{ $case->description }}

                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
@endsection
