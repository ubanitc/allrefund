@extends('layouts.home')

@section('content')
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s" data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                    <img src="{{ asset('assets/images/left-image.png') }}" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
                <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="left-heading">
                        <h5>Private Investigations for Individuals</h5>
                    </div>
                    <div class="left-text">
                        <p>Once you’ve exhausted your skills as an amateur sleuth, it may be time to hire a professional. Retaining an investigator is not something that most people do everyday, which is why we walk you through everything step-by-step from the initial engagement agreement, through our investigative report and phone call while maintaining a completely transparent process.<br><br></p>

@endsection
