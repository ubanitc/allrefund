@extends('layouts.home')
@section('content')
<section class="section" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s" data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                <img src="{{ asset('assets/images/left-image.png') }}" class="rounded img-fluid d-block mx-auto" alt="App">
            </div>
            <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                <div class="left-heading">
                    <h5>Investigative Services for Businesses &amp; Corporations</h5>
                </div>
                <div class="left-text">
                    <p>From conducting comprehensive background investigations on new executives, to interviewing witnesses for an internal investigation, to gathering intelligence about competitors, we help you gather critical information to help you make a more informed business decision.<br><br></p>
                    <p></p><p></p><h5>We Help You Make More Informed Business Decisions</h5><p></p>
                    <p>From conducting comprehensive background investigations on new executives, to interviewing witnesses for an internal investigation, to gathering intelligence about competitors, we help you gather critical information to help you make a more informed business decisions.

                        We act fast to help take preemptive action to limit expensive problems or understand the extent of the issues at hand.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="hr"></div>
            </div>
        </div>
    </div>
</section>
@endsection
