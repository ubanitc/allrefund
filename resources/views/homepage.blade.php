@extends('layouts.home')
@section('content')

    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area" id="welcome">

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="left-text col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                        <h1>WE FIND INFO THAT NORMAL PEOPLE <strong>CAN’T</strong></h1>
                        <p>To help our clients avoid costly ones</p>
                        <a href="#about" class="main-button-slider">Who we help</a>
                        <a href="{{ route('register') }}" class="main-button-slider">Get Your Money Back</a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <img src="{{ asset('assets/images/slider-icon.png') }}" class="rounded img-fluid d-block mx-auto" alt="Allrefund">
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->


    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="{{ asset('assets/images/left-image.png') }}" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
                <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="left-heading">
                        <h5>Why We’re Different</h5>
                    </div>
                    <div class="left-text">
                        <p>While many private investigators are considered to be intelligent and crafty, they can also be secretive, sleuthy, unreliable and stuck in the dark ages. If you don’t believe us, you can check out the public perception of a private
                            investigator. <br>Some investigators like to perpetuate the “cloak and dagger” image with their secret sources, inside intelligence and years of law enforcement experience.<br>We are not one of them…. <br><br></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section" id="services">
        <div class="container">
            <div class="row" style="margin-bottom: 29px;">
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <span style="text-align:center;color: white;"><h5 style="font-size: 27px;">Who We Help</h5></span>
                </div>

            </div>
            <div class="row">

                <div class="owl-carousel owl-theme">
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('assets/images/service-icon-01.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Individuals</h5>
                        <p>Whether it’s reuniting you with your biological parents, tracking down familial information or conducting background checks, we can help.</p>
                        <a href="{{ route('individuals') }}" class="main-button">Read More</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('assets/images/service-icon-02.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Investors</h5>
                        <p>Do you really know who you are investing with? We’ll make sure that you are not getting mixed up in the next Bernie Madoff. </p>
                        <a href="{{ route('investors') }}" class="main-button">Read More</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('assets/images/service-icon-03.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Law Firms</h5>
                        <p>We find creative and legal ways to best your adversary – not off-the-shelf ideas in the legal gray area that could threaten your lawsuit.</p>
                        <a href="{{ route('law-firms') }}" class="main-button">More Detail</a>
                    </div>
                    <div class="item service-item">
                        <div class="icon">
                            <i><img src="{{ asset('assets/images/service-icon-02.png') }}" alt=""></i>
                        </div>
                        <h5 class="service-title">Businesses</h5>
                        <p>We act fast to help take preemptive action to limit expensive problems or understand the extent of the issues at hand.</p>
                        <a href="{{ route('businesses') }}" class="main-button">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->


    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about2">
        <div class="container">
            <div class="row">
                <div class="left-text col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix">
                    <div class="left-heading">
                        <h5>When to Contact Us</h5>
                    </div>
                    <p></p>
                    <ul>
                        <li>
                            <img src="{{ asset('assets/images/about-icon-01.png') }}" alt="">
                            <div class="text">
                                <h6>I need to locate a difficult-to-find individual, relative or associate</h6>
                            </div>
                        </li>
                        <li>
                            <img src="{{ asset('assets/images/about-icon-02.png') }}" alt="">
                            <div class="text">
                                <h6>I need to gather intelligence against a legal adversary</h6>
                            </div>
                        </li>
                        <li>
                            <img src="{{ asset('assets/images/about-icon-03.png') }}" alt="">
                            <div class="text">
                                <h6>I need assistance in locating witnesses and conducting interviews</h6>
                            </div>
                        </li>
                        <li>
                            <img src="{{ asset('assets/images/about-icon-02.png') }}" alt="">
                            <div class="text">
                                <h6>I need a background check on a C-level executive, not just a run-of-the-mill background check</h6>
                            </div>
                        </li>
                        <li>
                            <img src="{{ asset('assets/images/about-icon-01.png') }}" alt="">
                            <div class="text">
                                <h6>I need to determine if it’s worthwhile to file a lawsuit</h6>
                            </div>
                        </li>
                        <li>
                            <img src="{{ asset('assets/images/about-icon-02.png') }}" alt="">
                            <div class="text">
                                <h6>I need to make sure my potential business partners don’t have any skeletons</h6>
                            </div>
                        </li>
                        <li>
                            <img src="{{ asset('assets/images/about-icon-03.png') }}" alt="">
                            <div class="text">
                                <h6>I have lost some money or cryptocurrency online to impostors</h6>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="right-image col-lg-7 col-md-12 col-sm-12 mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <img src="{{ asset('assets/images/right-image.png') }}" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->






    <!-- ***** Frequently Question Start ***** -->
    <section class="section" id="frequently-question">
        <div class="container">
            <!-- ***** Section Title Start ***** -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading" style="text-align:center;margin-bottom: 30px;">
                        <h5>How We Are Different</h5>
                    </div>
                </div>
                <div class="offset-lg-12 col-lg-12">
                    <div class="">
                        <p>Some investigators like to perpetuate the “cloak and dagger” image with their secret sources, inside intelligence and years of law enforcement experience.
                            <br>We are not one of them. There are no cloaks and daggers here. We’ve built an investigative firm based on transparency, openness and simply doing great work. Here’s why we are different:
                        </p>
                    </div>
                </div>
            </div>
            <!-- ***** Section Title End ***** -->

            <div class="row">
                <div class="left-text col-lg-6 col-md-6 col-sm-12">
                    <h5>Sound like the kind of investigative agency you’re looking for?</h5>
                    <div class="accordion-text">
                        <p>Our regular clientele includes national and international law firms, financial institutions as well as private individuals.</p>
                        <a href="{{ route('login') }}" class="main-button">Request a Consultation</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="accordions is-first-expanded">
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Creative</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>People don’t come to us for off-the shelf ideas. We help you find legal and ethical solutions to your specific problems.
                                    </p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Dig Deep</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>If you can find something easily on Google, you probably don’t need to hire us. We know where to look and we dig deep to find what we need.
                                    </p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Ethical</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>We don’t use illegal or unethical methods of obtaining information. If you need someone else’s bank accounts information, phone records, emails or medical records, you are wasting your time with us.
                                    </p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Transparent</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>Our approach is simple and transparent. We use open sources, public records and thoughtful and creative investigative techniques to get answers.
                                    </p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Open</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>We have invested tremendous effort in educating you, the client, so you can be your own amateur P.I. But when it comes to needing a professional, we are always available.</p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Honest</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>If we can’t help you, we’ll tell you, or help you find someone who is better suited. We are not in the business of making promises we can’t live up to.</p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Resourceful</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>We don’t just rely on 21st century technology, we use old school “gumshoe” techniques and the most important tool around: our brain.</p>
                                </div>
                            </div>
                        </article>
                        <article class="accordion">
                            <div class="accordion-head">
                                <span>Commitment</span>
                                <span class="icon">
                                    <i class="icon fa fa-chevron-right"></i>
                                </span>
                            </div>
                            <div class="accordion-body">
                                <div class="content">
                                    <p>We have a tenacious appetite to get answers and we are committed to helping our clients.</p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Frequently Question End ***** -->

    <!-- ***** Frequently Question Start ***** -->
    <section class="section" id="testimonials">
        <div class="container">
            <!-- ***** Section Title Start ***** -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading" style="text-align:center;margin-bottom: 30px;">
                        <h5>Leave us a feedback</h5>
                    </div>
                </div>
                <div class="offset-lg-12 col-lg-12">
                    <div class="">
                        <div id="disqus_thread"></div>
                        <script>
                            /**
                             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
                            /*
                            var disqus_config = function () {
                            this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                            this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };
                            */
                            (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document,
                                    s = d.createElement('script');
                                s.src = 'https://avast-security.disqus.com/embed.js';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    </div>
                </div>
            </div>
            <!-- ***** Section Title End ***** -->


        </div>
    </section>
    <!-- ***** Frequently Question End ***** -->




    <!-- ***** Contact Form Start ***** -->
    <section id="contact-us">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="section-heading" style="text-align:center;margin-bottom: 30px;">
                <h5>Request a Consultation</h5>
            </div>
            <div class="contact-form">
                <form id="contact" action="{{ route('contact-form') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <fieldset>
                                <input name="name" type="text" id="name" placeholder="Full Name" required="" class="contact-field">
                            </fieldset>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <fieldset>
                                <input name="email" type="text" id="email" placeholder="E-mail" required="" class="contact-field">
                            </fieldset>
                        </div>
                        <div class="col-lg-12">
                            <fieldset>
                                <textarea name="message" rows="6" id="message" placeholder="Your Message" required="" class="contact-field"></textarea>
                            </fieldset>
                        </div>
                        <div class="col-lg-4">
                        </div>
                        <div class="col-lg-4">
                            <fieldset>
                                <input type="submit" id="form-submit" name="form-submit" class="thebutton main-button" value="Send It" />
                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- ***** Contact Form End ***** -->

    <!-- ***** Contact Us End ***** -->

@endsection
