@extends('layouts.home')
@section('content')
<section class="section" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s" data-scroll-reveal-id="1" data-scroll-reveal-initialized="true" data-scroll-reveal-complete="true">
                <img src="{{ asset('assets/images/left-image.png') }}" class="rounded img-fluid d-block mx-auto" alt="App">
            </div>
            <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                <div class="left-heading">
                    <h5>Investigative Services for Investors</h5>
                </div>
                <div class="left-text">
                    <p>Do you really know who you are investing with? As a family office, private equity firm or individual investor, you have completed all of your due diligence, dotted the i’s and crossed the t’s, but how can you be sure that you are not getting mixed up in the next Bernie Madoff?<br><br></p><h5>We Make Sure There Are No Skeletons Hiding in the Closet</h5><p> They say that past performance does not guarantee future results, but would you probably would not invest with someone whose past performance has a history of embellishment, criminal or regulatory infractions, harassment or litigiousness.</p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="hr"></div>
            </div>
        </div>
    </div>
</section>
@endsection
