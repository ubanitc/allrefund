@extends('layouts.admindash')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Case {{ $case->id }}</h1>
                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row card card-body">
                    <!-- ./col -->
                   <h3> Case Title: {{ $case->title }}</h3>
                    <h4>Status: {{ $case->status }}</h4>
                    <p><strong>Description:</strong> {{ $case->description }}</p>
                    <!-- ./col -->


                    <p>
                        @if($case->status == 'Open' )
                            <a href="{{ route('admin.take-case', $case->id) }}"  class="btn btn-success">Take Case</a>

                        @endif
                        @if($case->status != 'Closed')
                                <a href="{{ route('admin.close-case', $case->id) }}" class="btn btn-danger">Close Case</a>

                            @endif
                    </p>
                </div>
                <!-- /.row -->
                <!-- Main row -->
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
