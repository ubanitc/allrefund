@extends('layouts.admindash')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">All Cases</h1>
                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Open / Ongoing Cases</h3>


                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($cases as $index => $case)

                                        <tr>
                                            <td>{{ $index +1 }}</td>
                                            <td>{{ $case->user->first_name }} {{ $case->user->last_name }}</td>
                                            <td>{{ $case->user->email }}</td>
                                            <td>{{ $case->title }}</td>
                                            <td>{{ Str::words($case->description, 6, '....') }}</td>
                                            <td>{{ $case->status }}</td>
                                            <td>{{ $case->created_at }}</td>

                                            <td><a class="btn btn-success d-inline" href="{{ route('admin.all-cases-page', $case->id) }}">View</a><button class="btn btn-danger d-inline ml-2" onclick="event.preventDefault(); document.getElementById('delete-case').submit() ">Delete</button></td>
                                            <form action="{{ route('admin.delete-case', $case->id) }}" id="delete-case" method="post">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </tr>

                                    @empty
                                        <tr aria-colspan="7">No Open Case Available</tr>
                                    @endforelse


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->

                <br>
                <br>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Closed Cases</h3>


                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($closed as $index => $case)

                                        <tr>
                                            <td>{{ $index +1 }}</td>
                                            <td>{{ $case->user->first_name }} {{ $case->user->last_name }}</td>
                                            <td>{{ $case->user->email }}</td>
                                            <td>{{ $case->title }}</td>
                                            <td>{{ Str::words($case->description, 6, '....') }}</td>
                                            <td>{{ $case->status }}</td>
                                            <td>{{ $case->created_at }}</td>

                                            <td><button class="btn btn-success d-inline" onclick="event.preventDefault(); document.getElementById('reopen-case').submit() ">Re-Open</button><button class="btn btn-danger d-inline ml-2" onclick="event.preventDefault(); document.getElementById('delete-case-2').submit() ">Delete</button></td>
                                            <form action="{{ route('admin.reopen-case', $case->id) }}" id="reopen-case" method="post">
                                                @csrf
                                                @method('PATCH')
                                            </form>
                                            <form action="{{ route('admin.delete-case', $case->id) }}" id="delete-case-2" method="post">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </tr>

                                    @empty
                                        <tr aria-colspan="7">No Closed Case Available</tr>
                                    @endforelse


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- Main row -->
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('custom-script')
    <script>
        let table = new DataTable('#myTable');
    </script>
@endpush
