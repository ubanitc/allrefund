<?php

namespace App\Http\Controllers;

use App\Models\Caseopen;
use App\Models\User;
use App\Notifications\StatusChangeNotification;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard()
    {
        $users = User::all()->count();

        $cases = Caseopen::all()->count();


        return view('admin.dashboard',[
            'user_count'=> $users - 1,
            'cases_count' => $cases
        ]);
    }

    public function all_users()
    {
        $users = User::where('isAdmin', 0)->get();
        return view('admin.all-users', compact('users'));
    }

    public function delete_user(User $id)
    {
        $id->delete();

        return redirect()->back();
    }

    public function all_cases()
    {
        $cases = Caseopen::where('status','Open')->orWhere('status','Active')->get();
        $closed = Caseopen::where('status','Closed')->get();
        return view('admin.all-cases', compact('cases','closed'));
    }

    public function reopen_case(Caseopen $id)
    {
            $message = 'Your Case with ID '. $id->id . 'has been re-opened';
        $id->status = 'Active';
        $id->save();

        $id->user->notify(new StatusChangeNotification($message));

        return redirect()->back();
    }

    public function delete_case(Caseopen $id)
    {
        $id->delete() ;

        return redirect()->back();
    }
    public function take_case(Caseopen $id)
    {
        $message = 'Your Case with ID '. $id->id . 'is now active';

        $id->status = 'Active';

        $id->save();
        $id->user->notify(new StatusChangeNotification($message));

        return redirect()->back();
    }
    public function close_case(Caseopen $id)
    {
        $message = 'Your Case with ID '. $id->id . 'has been closed';

        $id->status = 'Closed';
        $id->save();
        $id->user->notify(new StatusChangeNotification($message));

        return redirect()->back();
    }

    public function all_cases_page(Caseopen $id){
        return view('admin.all-cases-page', ['case'=>$id]);
    }
}
