<?php

namespace App\Http\Controllers;

use App\Models\Caseopen;
use App\Models\User;
use App\Notifications\OpenTicketNotify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $case = Caseopen::where('user_id', Auth::id())->get();
        $open = $case->where('status','Open');
        $closed = $case->where('status','Closed');


        return view('user.dashboard', compact('open','closed','case'));
    }

    public function self_service()
    {
        return view('user.self-service');
    }

    public function self_service_post(Request $request){

        $user = Auth::user();

        $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'phone_number' => ['required'],
            'city' => ['required'],
            'country' => ['required'],
        ]);


        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->city = $request->city;
        $user->phone_number = $request->phone_number;
        $user->country = $request->country;

        $user->save();

        return redirect()->back();
    }

    public function open_case(){
        return view('user.open-case');
    }

    public function open_case_post(Request $request){
        $case = Caseopen::create([
            'title' => $request->title,
            'description' => $request->message,
            'user_id' => Auth::id(),
        ]);

        $user = User::where('isAdmin',1)->first();

        $user->notify(new OpenTicketNotify());
        session()->flash('success-case','Report Has been created Successfully');

        return redirect()->route('user.all-cases');
    }

    public function all_cases(){

        $cases = Caseopen::where('user_id',Auth::id())->get();

        return view('user.all-cases', compact('cases'));
    }

    public function all_cases_page(Caseopen $id){

        if (!$id){
            abort(404);
        }
        $case = $id;

        return view('user.all-cases-page', compact('case'));

    }
}
