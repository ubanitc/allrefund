<?php

use App\Models\User;
use App\Notifications\ContactFormNotification;
use App\Notifications\OpenTicketNotify;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('homepage');
})->name('home');

Route::view('/individuals', 'individuals')->name('individuals');
Route::view('/investors', 'investors')->name('investors');
Route::view('/law_firms', 'law-firms')->name('law-firms');
Route::view('/businesses', 'businesses')->name('businesses');
Route::view('/blacklist', 'blacklist')->name('blacklist');
Route::post('/contact-form', function (Request $request){
    $user = User::where('isAdmin',1)->first();

    $user->notify(new ContactFormNotification($request->name, $request->email, $request->message));
    return redirect()->route('home');
})->name('contact-form');

Route::get('/artisan', function (){
    $clear_cache = Artisan::call('route:clear');
    $clear_cache = Artisan::call('config:clear');
    $clear_cache = Artisan::call('view:clear');
    $clear_cache = Artisan::call('optimize');

    echo "done all";
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//user routes


Route::group([
    'as' => 'user.',
    'middleware' => ['isUser','auth']
], function (){
    Route::get('dashboard', [\App\Http\Controllers\UserController::class,'index'])->name('dashboard');
    Route::get('self-service', [\App\Http\Controllers\UserController::class,'self_service'])->name('self-service');
    Route::post('self-service-post', [\App\Http\Controllers\UserController::class,'self_service_post'])->name('self-service-post');
    Route::get('open-case', [\App\Http\Controllers\UserController::class,'open_case'])->name('open-case');
    Route::post('open-case-post', [\App\Http\Controllers\UserController::class,'open_case_post'])->name('open-case-post');
    Route::get('all-cases', [\App\Http\Controllers\UserController::class,'all_cases'])->name('all-cases');
    Route::get('all-cases/{id}', [\App\Http\Controllers\UserController::class,'all_cases_page'])->name('all-cases-page');
    Route::get('all-users', [\App\Http\Controllers\UserController::class,'all_users'])->name('all-users');
});

//admin routes
Route::group([
    'as' => 'admin.',
    'middleware' => ['isAdmin','auth'],
    'prefix' => 'admin'
], function (){
    Route::get('/dashboard', [\App\Http\Controllers\AdminController::class,'dashboard'])->name('dashboard');
    Route::get('all-users', [\App\Http\Controllers\AdminController::class,'all_users'])->name('all-users');
    Route::get('all-cases', [\App\Http\Controllers\AdminController::class,'all_cases'])->name('all-cases');
    Route::delete('/delete-user/{id}', [\App\Http\Controllers\AdminController::class,'delete_user'])->name('delete-user');
    Route::patch('/reopen-casereopen-case/{id}', [\App\Http\Controllers\AdminController::class,'reopen_case'])->name('reopen-case');
    Route::delete('/delete-case-case/{id}', [\App\Http\Controllers\AdminController::class,'delete_case'])->name('delete-case');
    Route::get('all-cases/{id}', [\App\Http\Controllers\AdminController::class,'all_cases_page'])->name('all-cases-page');
    Route::get('/take-case/{id}', [\App\Http\Controllers\AdminController::class, 'take_case'])->name('take-case');
    Route::get('/close-case/{id}', [\App\Http\Controllers\AdminController::class, 'close_case'])->name('close-case');

});
